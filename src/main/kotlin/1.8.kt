import java.util.*

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/19
* TITLE: Dobla el decimal
*/
fun main(){
    val scan = Scanner(System.`in`).useLocale(Locale.UK)
    println("valor para multiplicar?")
    val userInputValue:Double = scan.nextDouble()
    println(calculaDobleDecimal(userInputValue))

}

fun calculaDobleDecimal(uiv1: Double): Double {
    return uiv1*2.0
}