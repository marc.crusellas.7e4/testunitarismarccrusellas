import java.util.Scanner

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/19
* TITLE: Pupitres
*/
fun main() {
    val scan = Scanner(System.`in`)

    print("1r class? ")
    val uiv1 = scan.nextInt()

    print("2n class? ")
    val uiv2 = scan.nextInt()

    print("3r class? ")
    val uiv3 = scan.nextInt()


    println(countPupitres(uiv1, uiv2, uiv3))
}
fun countPupitres(uiv1: Int, uiv2: Int, uiv3:Int): Int{
    var table = 0
    var classes: Array<Int> = arrayOf(uiv1,uiv2, uiv3)
    for (k in classes){
        for (i in k downTo 0 step 2) {
            when (i){
                0 -> {
                }
                else -> {
                    table++
                }
            }

        }
    }

    return table
}