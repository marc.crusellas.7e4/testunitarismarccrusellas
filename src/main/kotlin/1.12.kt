import java.lang.Math
import java.util.*

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/20
* TITLE: De Celsius a Fahrenheit
*/
fun main(){
    val scan = Scanner(System.`in`).useLocale(Locale.UK)
    println("A que temperatura estas en graus Celsius?")
    var uiv:Double = scan.nextDouble()
    println(uiv.toFahrenheit())

}

fun Double.toFahrenheit() = (this*1.8)+32