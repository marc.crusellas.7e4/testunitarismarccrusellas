import java.util.*

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/21
* TITLE: Afegeix un segon
*/
fun main(){
    val scan = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introduir el número")
    var uiv1: Int = scan.nextInt()

    println("${uiv1.sum1Sec()}")

}

fun Int.sum1Sec(): Int{
    val uiv:Int
    if (this+1>59){
        return 0
    } else {
        return this+1
    }
}