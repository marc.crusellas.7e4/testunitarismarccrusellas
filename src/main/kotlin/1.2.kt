import java.util.Scanner

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/19
* TITLE: Dobla l’enter
*/
fun main(){
    val scan = Scanner(System.`in`)
    println("valor para multiplicar?")
    val userInputValue = scan.nextInt()
    println(doblaEnter(userInputValue))

}
fun doblaEnter(num:Int):Int{
    return num*2
}

