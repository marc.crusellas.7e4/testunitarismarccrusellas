import java.lang.Math
import java.text.DecimalFormat
import java.util.*
import kotlin.math.pow

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/20
* TITLE: Quina és la mida de la meva pizza?
*/
fun main(){
    val scan = Scanner(System.`in`).useLocale(Locale.UK)
    println("Qual es el diametro?")
    var uiv:Double = scan.nextDouble()
    println(uiv.midaPizza())

}

fun Double.midaPizza(): Double {

    val df = DecimalFormat("#.##")
    return df.format(((this / 2).pow(2.0))*Math.PI).replace(",", ".").toDouble()


}
