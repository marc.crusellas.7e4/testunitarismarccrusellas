import java.util.Scanner

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/19
* TITLE: Operació boja
*/
fun main(){
    val scan = Scanner(System.`in`)

    print("1r valor? ")
    val userInputValue1 = scan.nextInt()

    print("2n valor? ")
    val userInputValue2 = scan.nextInt()

    print("3r valor? ")
    val userInputValue3 = scan.nextInt()

    print("4t valor? ")
    val userInputValue4 = scan.nextInt()


    println(operacioBoja(userInputValue1, userInputValue2, userInputValue3, userInputValue4))

}

fun operacioBoja(num1:Int, num2:Int, num3:Int, num4:Int): Int{
    return (num1+num2)*(num3%num4)
}