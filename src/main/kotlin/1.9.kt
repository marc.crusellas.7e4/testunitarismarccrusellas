import java.lang.Math.abs
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*
import kotlin.math.roundToInt

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/19
* TITLE: Calcula el descompte
*/
fun main() {
    val scan = Scanner(System.`in`).useLocale(Locale.UK)
    println("Quin era el preu?")
    val uiv1: Double = scan.nextDouble()

    println("Quin es el preu?")
    val uiv2: Double = scan.nextDouble()

    println("${uiv1 calcDescompte uiv2} %")

}

infix fun Double.calcDescompte(es: Double): Double {

    val df = DecimalFormat("#.##")
    return df.format((100 - (100 / (this / es)))).replace(",", ".").toDouble()


}
