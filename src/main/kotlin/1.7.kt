import java.util.Scanner

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/19
* TITLE: Número següent
*/
fun main(){
    val scan = Scanner(System.`in`)
    print("valor? ")

    val userInputValue1 = scan.nextInt()


    println("Després ve el "+numeroSeguent(userInputValue1))

}

fun numeroSeguent(uiv1: Int): Int{
    return uiv1+1
}