import java.lang.Math
import java.util.*

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/20
* TITLE: Calculadora de volum d’aire
*/
fun main(){
    val scan = Scanner(System.`in`).useLocale(Locale.UK)
    println("1r mesura?")
    var uiv1:Double = scan.nextDouble()

    println("2n mesura?")
    var uiv2:Double = scan.nextDouble()

    println("3r mesura?")
    var uiv3:Double = scan.nextDouble()

    val uiv = uiv1*uiv2*uiv3
    println(volumAire(uiv1, uiv2, uiv3))

}

fun volumAire(uiv1: Double,uiv2: Double, uiv3: Double): Double{
    return uiv1*uiv2*uiv3
}