import java.util.Scanner

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/19
* TITLE: Suma de dos nombres enters
*/
fun main(){
    val scan = Scanner(System.`in`)
    print("1r valor per sum? ")

    val userInputValue1 = scan.nextInt()
    print("2n valor per sum? ")
    val userInputValue2 = scan.nextInt()

    println(sumaDeDosNombresEnters(userInputValue1, userInputValue2))

}

fun sumaDeDosNombresEnters(num1: Int, num2: Int): Int{
    return num1+num2
}