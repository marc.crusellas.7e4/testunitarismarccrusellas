import java.util.*

/*
* AUTHOR: Marc Crusellas Serra
* DATE: 2022/09/20
* TITLE: Quina temperatura fa?
*/
fun main(){
    val scan = Scanner(System.`in`).useLocale(Locale.UK)
    println("Temperatura?")
    var uiv1:Double = scan.nextDouble()

    println("Increment?")
    var uiv2:Double = scan.nextDouble()

    println("La temperatura actual és ${uiv1 sumTemperatura uiv2}º")

}

infix fun Double.sumTemperatura(sum: Double) = this+sum