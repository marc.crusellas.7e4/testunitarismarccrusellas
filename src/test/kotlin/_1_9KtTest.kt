package c1

import calcDescompte
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import kotlin.math.abs

internal class _1_9KtTest {

    @Test
    fun calcDescompteTest1() {
        assertEquals(20.0, 1000.0 calcDescompte 800.0)

    }
    @Test
    fun calcDescompteTest2() {
        assertEquals(22.22, 540.0 calcDescompte 420.0)

    }
    @Test
    fun calcDescompteTest3() {
        assertEquals(-44.0, 500.0 calcDescompte 720.0)

    }
}