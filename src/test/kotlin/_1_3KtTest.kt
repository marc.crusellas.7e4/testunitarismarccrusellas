package c1

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import sumaDeDosNombresEnters

internal class _1_3KtTest {

    @Test
    fun sumaDeDosNombresEnters_103_Test1() {
        assertEquals(4, sumaDeDosNombresEnters(2,2))
    }

    @Test
    fun sumaDeDosNombresEnters_103_Test2() {
        assertEquals(3, sumaDeDosNombresEnters(-1,4))
    }

    @Test
    fun sumaDeDosNombresEnters_103_Test3() {
        assertEquals(-1098, sumaDeDosNombresEnters(-1000,-98))
    }
}