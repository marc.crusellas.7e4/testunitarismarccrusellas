package c1

import calculaDobleDecimal
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_8KtTest {

    @Test
    fun calculaDobleDecimalTest1() {
        assertEquals(4.2, calculaDobleDecimal(2.1))
    }
    @Test
    fun calculaDobleDecimalTest2() {
        assertEquals(246.912, calculaDobleDecimal(123.456))

    }
    @Test
    fun calculaDobleDecimalTest3() {
        assertEquals(643.308, calculaDobleDecimal(321.654))

    }

}