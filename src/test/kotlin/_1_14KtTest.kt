package c1

import aPagar
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_14KtTest {

    @Test
    fun `aPagar Test 1`() {
        assertEquals(19.14, 4 aPagar  76.56)
    }
    @Test
    fun `aPagar Test 2`() {
        assertEquals(30.4875, 8 aPagar 243.90)
    }
    @Test
    fun `aPagar Test 3`() {
        assertEquals(767.150202020202, 99 aPagar  75947.87)
    }
}