package c1

import numeroSeguent
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest{
    @Test
    fun numeroSeguent_107_Test1() {
        assertEquals(21, numeroSeguent(20))
    }

    @Test
    fun numeroSeguent_107_Test2() {
        assertEquals(932876, numeroSeguent(932875))
    }

    @Test
    fun numeroSeguent_107_Test3() {
        assertEquals(-19, numeroSeguent(-20))
    }
}