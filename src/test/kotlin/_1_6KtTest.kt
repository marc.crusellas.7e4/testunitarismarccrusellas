package c1

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import countPupitres

internal class _1_6KtTest {

    @Test
    fun countPupitres_106_Test1() {
        assertEquals(32, countPupitres(20,21,21))
    }

    @Test
    fun countPupitres_106_Test2() {
        assertEquals(37, countPupitres(22,25,26))
    }
    @Test
    fun countPupitres_106_Test3() {
        assertEquals(41, countPupitres(31,29,19))
    }

}