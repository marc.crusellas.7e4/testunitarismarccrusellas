package c1

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import personaltToDouble

internal class _1_16KtTest {

    @Test
    fun `personaltToDouble Test 1`() {
        assertEquals(24.0, 24.personaltToDouble())
    }
    @Test
    fun `personaltToDouble Test 2`() {
        assertEquals(24.0, 24.personaltToDouble())
    }
    @Test
    fun `personaltToDouble Test 3`() {
        assertEquals(5.0, 5.personaltToDouble())
    }
}