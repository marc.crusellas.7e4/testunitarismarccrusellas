package c1

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import volumAire

internal class _1_11KtTest {

    @Test
    fun `volumAire Test 1`() {
        assertEquals(69.19200000000001, volumAire(7.2, 3.1, 3.1))
    }
    @Test
    fun `volumAire Test 2`() {
        assertEquals(40.0, volumAire(5.0, 4.0, 2.0))

    }
    @Test
    fun `volumAire Test 3`() {
        assertEquals(282752.0, volumAire(4.0, 94.0, 752.0))

    }
}