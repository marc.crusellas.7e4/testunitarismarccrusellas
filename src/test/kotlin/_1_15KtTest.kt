package c1

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import sum1Sec

internal class _1_15KtTest {

    @Test
    fun `sum1Sec Test 1`() {
        assertEquals(41, 40.sum1Sec())
    }
    @Test
    fun `sum1Sec Test 2`() {
        assertEquals(0, 59.sum1Sec())
    }
    @Test
    fun `sum1Sec Test 3`() {
        assertEquals(1, 0.sum1Sec())
    }
}