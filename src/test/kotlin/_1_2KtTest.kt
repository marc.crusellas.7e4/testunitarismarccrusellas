package c1

import doblaEnter
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2KtTest{


    @Test
    fun e1_2test1(){
        assertEquals(4, doblaEnter(2))
    }

    @Test
    fun e1_2test2(){
        assertEquals(18, doblaEnter(9))
    }
    @Test
    fun e1_2test3(){
        assertEquals(-2, doblaEnter(-1))
    }

}