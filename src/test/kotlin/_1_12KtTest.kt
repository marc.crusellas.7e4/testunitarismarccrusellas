package c1

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import toFahrenheit

internal class _1_12KtTest {

    @Test
    fun `toFahrenheit Test 1`() {
        assertEquals(91.58000000000001, 33.1.toFahrenheit())

    }
    @Test
    fun `toFahrenheit Test 2`() {
        assertEquals(26.24, (-3.2).toFahrenheit())

    }
    @Test
    fun `toFahrenheit Test 3`() {
        assertEquals(462.2, 239.0.toFahrenheit())

    }

}