package c1

import operacioBoja
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_5KtTest {

    @Test
    fun operacioBoja_105_Test1() {
        assertEquals(9, operacioBoja(1,2,3,4))
    }

    @Test
    fun operacioBoja_105_Test2() {
        assertEquals(460, operacioBoja(78,-58,23,500))
    }
    @Test
    fun operacioBoja_105_Test3() {
        assertEquals(-156016, operacioBoja(-245,643,-856,464))
    }

}