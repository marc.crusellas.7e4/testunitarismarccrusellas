package c1

import calculaArea
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTest{

    @Test
    fun calculaArea_104_Test1(){
        assertEquals(32, calculaArea(8,4))
    }
    @Test
    fun calculaArea_104_Test2(){
        assertEquals(46, calculaArea(1,46))
    }

    @Test
    fun calculaArea_104_Test3(){
        assertEquals(10092, calculaArea(2523,4))
    }

}