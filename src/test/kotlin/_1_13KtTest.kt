package c1

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import sumTemperatura

internal class _1_13KtTest {

    @Test
    fun `sumTemperatura Test 1`() {
        assertEquals(31.4, 28.4 sumTemperatura 3.0)
    }
    @Test
    fun `sumTemperatura Test 2`() {
        assertEquals(40.1, 35.6 sumTemperatura 4.5)
    }
    @Test
    fun `sumTemperatura Test 3`() {
        assertEquals(764.0, 700.0 sumTemperatura 64.0)
    }

}