package c1

import midaPizza
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class _1_10KtTest {

    @Test
    fun `midaPizza Test 1`() {
        assertEquals(1134.11, 38.0.midaPizza())

    }
    @Test
    fun `midaPizza Test 2`() {
        assertEquals(2375.83, 55.0.midaPizza())

    }
    @Test
    fun `midaPizza Test 3`() {
        assertEquals(785398.16, 1000.0.midaPizza())

    }
}